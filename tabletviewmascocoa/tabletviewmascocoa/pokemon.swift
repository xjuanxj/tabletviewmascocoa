//
//  pokemon.swift
//  tabletviewmascocoa
//
//  Created by Juan Sebastian Benavides Cardenas on 27/11/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import Foundation
import ObjectMapper
class pokemon:Mappable
{
    var pkName:String?
    var pkWeight:Double?
    var pkheight:Double?
    var pkId:Int?
    var pkAbility:String?
    var pkMove:String?
    var pkType:String?
    
required init?(map: Map){
    
}
func mapping(map: Map) {
    pkId <- map["id"]
    pkName <- map["name"]
    pkWeight <- map["weight"]
    pkheight <- map["height"]
    pkAbility <- map["abilities.0.ability.name"]
    pkMove <- map["moves.0.move.name"]
    pkType <- map["types.0.type.name"]
}
}
