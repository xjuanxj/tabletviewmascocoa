//
//  PokeDexViewController.swift
//  tabletviewmascocoa
//
//  Created by Juan Sebastian Benavides Cardenas on 5/1/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit


class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{
    
    let pokemonService = PokemonService()
    var pokemonArray:[pokemon] = []
    var selectedPokemon = 0 // indice selecionador de fila
    
    @IBOutlet weak var tableViewPokemon: UITableView!
    
    override func viewDidLoad() {
            super.viewDidLoad()
            pokemonService.delegate = self // llamado al delegado
            // Do any additional setup after loading the view.
        }
        func numberOfSections(in tableView: UITableView) -> Int { // metodo para hacer la tabla
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            switch section {
            case 0:
                return pokemonArray.count //igual al lenght de java en arrays
            default:
                return 5
            }
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
            UITableViewCell {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
                cell.fillData(pokemonArray[indexPath.row])
                //cell.textLabel?.text = pokemonArray[indexPath.row].pkName //como
                return cell
                
        }
        
    
    override func viewWillAppear(_ animated: Bool) // ejecuta el delegado
    {
        
        pokemonService.get20FirtsPokemon()
    }
    
    func first20Pokemon(_ pokemon: [pokemon]) // implementacion del delegado
    {
        pokemonArray = pokemon
        tableViewPokemon.reloadData()
    }
    //funcion para obetener la fila seleccionada Puede ser will select o did select
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath)-> IndexPath?
    {
        selectedPokemon = indexPath.row // obtencion del row seleccionado
        return indexPath
    }
    
    //Metodo que permite pasar datos entre vistas (view controler)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let destination = segue.destination as! PokemonDetailViewController
        
        destination.pokemon = pokemonArray[selectedPokemon]
    }
    
        
        
        
        
    }


