//
//  PokemonTableViewCell.swift
//  tabletviewmascocoa
//
//  Created by Juan Sebastian Benavides Cardenas on 5/1/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var idlabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData(_ pokemon:pokemon)
    {
        idlabel.text = "\(pokemon.pkId ?? 0)"
        nameLabel.text = pokemon.pkName
    }

}
