//
//  PokemonDetailViewController.swift
//  tabletviewmascocoa
//
//  Created by Juan Sebastian Benavides Cardenas on 8/1/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit

class PokemonDetailViewController: UIViewController {

    @IBOutlet weak var pokemonImage: UIImageView!
    
    
    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    var pokemon:pokemon?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pokemon?.pkName
        print(pokemon?.pkMove)

        // Do any additional setup after loading the view.
        let pkService = PokemonService()
        pkService.getPokemonImage((pokemon?.pkId)!) { (pkImage) in
            self.pokemonImage.image = pkImage
            
        }
        lbl.text=pokemon?.pkType
        lbl2.text = pokemon?.pkAbility
        lbl3.text = pokemon?.pkMove
        namelbl.text = "\(pokemon?.pkId ?? 0)"
    }
    

  
    

 

}
