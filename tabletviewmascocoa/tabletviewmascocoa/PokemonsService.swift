//
//  PokemonsService.swift
//  tabletviewmascocoa
//
//  Created by Juan Sebastian Benavides Cardenas on 5/1/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import AlamofireImage

//Creacion de delegados
//clase que realiza el trabajo
//POR ESTANDAR se coloca el nombre delegate
protocol PokemonServiceDelegate
{
    func first20Pokemon(_ pokemon: [pokemon])
    
    
    
}

//alamofire MVC
class PokemonService
{
    var delegate:PokemonServiceDelegate?
    
    
    func get20FirtsPokemon()
    {
        var pokemonArray:[pokemon] = []
        
        let group = DispatchGroup() //declaracion de un grupo de varios llamados asincronos
        
        
        for i in  1...3
        {
           group.enter()//sirve indicar el inicio de los llamados asyncronos
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)/").responseObject
                { (response: DataResponse<pokemon>)
                    in
                    
                    let pokemon = response.result.value
                    pokemonArray.append(pokemon!)
                    group.leave() // graba el registro del leave
                    
            }
            
                    
        }
        group.notify(queue: .main) // espera a que las notificacione
        {
           
            self.delegate?.first20Pokemon(pokemonArray.sorted(by: {$0.pkId! < $1.pkId!} // version reducida de  pokemonArray.sorted(by: { pk1, pk2) -> Bool in return pk1 < pk2
        )) //responde al hilo principal
    
        }
     
    }
    
    func getPokemonImage(_ id:Int, completion:@escaping (UIImage) -> ())
    {
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        Alamofire.request(url).responseImage{ response in
            debugPrint(response)
            
            
            
            if let image = response.result.value
            {
                completion(image)
                
            }
        }
    }
}
